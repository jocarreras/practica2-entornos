package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		// Da error en la linea 17
		// Tenemos que limpiar el teclado con input.nextLine(); porque sino
		//el programa da un error
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
