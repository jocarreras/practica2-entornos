package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		//metemos un numero por teclado, este numero tambien se utiliza para indicar
		//el recorrido del bucle donde finaliza, dentro del bucle se hace la operacion
		// numero introducido por teclado / i, si esta operacion da = 0, la variable
		//cantidadDivisores aumenta , al salir del bucle se comprueba cuantos divisores 
		// ha tenido como resultado, si tiene + de 2 divisores no es primo 
		// y si tiene menos de 2 divisores si lo es.
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
