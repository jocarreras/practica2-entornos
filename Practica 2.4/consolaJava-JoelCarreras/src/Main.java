import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in= new Scanner(System.in);
		int opcion; 
		System.out.println("***BIENVENIDO AL PROGRAMA DE ENTORNOS DE DESAROLLO***");
		System.out.println("Seleccione una de las siguientes opciones para continuar");
		do {
		System.out.println("1- Calcular el factorial de un numero");
		System.out.println("2-Crear una piramide ascendente a partir de un numero ");
		System.out.println("3-Calcular si un numero es primo o no");
		System.out.println("4-Calcular si un numero es par o no");
		System.out.println("5-Salir");
		opcion=in.nextInt();
		if(opcion<1 || opcion>5) {
			System.out.println("Error, introduce uno de los numeros seleccionados");
		}
		}while(opcion<1 || opcion>5);
		
		switch(opcion) {
		
		case 1: 
			System.out.println("Introduce el numero entero que quieres calcular");
			int numero1=in.nextInt();
			int factorial=factorial(numero1);
			System.out.println("El factorial de " + numero1 + " es " + factorial);
			break;
		
		case 2: 
			System.out.println("Introduce un numero ");
			int numero2=in.nextInt();
			dibujarPiramide(numero2);
			break;
			
		case 3:
			System.out.println("Introduce el numero que quieres calcular");
			int numero3=in.nextInt();
			int valor= esPrimo(numero3);
			if(valor==2){
				System.out.println("Es primo");
			}
			else{
				System.out.println("No es primo");
			}
			break;
		
		case 4: 
			esPar();
			break;
			
		case 5: 
			System.out.println("Hasta la proxima");
			System.exit(0);
			
			
		
		}

	}







	private static void esPar() {
		Scanner in= new Scanner (System.in);
		System.out.println("Introduce el numero que quieres averigurar si es par");
		int numero4=in.nextInt();
		if(numero4%2==0) {
			System.out.println("Es par");
		}
		else {
			System.out.println("Es impar");
		}
		
	}







	public static void dibujarPiramide(int numero2) {
		for(int linea=numero2;linea>=1;linea--){
    		for(int i=1;i<=linea;i++){
				System.out.print(linea);
    		}
		System.out.println();
    	}
	}





	public static int esPrimo(int numero3) {
		int contadorRestos=0;
		for(int i=1; i<=numero3; i++){
			if(numero3%i==0){
				contadorRestos++;
			}
		}
		
		return contadorRestos;
	}

	public static int factorial(int numero1) {
		int factorial=1;
		for(int i=1; i<=numero1; i++){
			factorial=factorial*i;
		}
		return factorial;
	}






		
}

