﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_JoelCarreras
{
   
    public class Program
    {

        public static void Main(String[] args)
        {
       
            int opcion;
            String opcionString;
            System.Console.WriteLine("***BIENVENIDO AL PROGRAMA DE ENTORNOS DE DESAROLLO***");
            System.Console.WriteLine("Seleccione una de las siguientes opciones para continuar");
            do
            {
                System.Console.WriteLine("1- Calcular el factorial de un numero");
                System.Console.WriteLine("2-Crear una piramide ascendente a partir de un numero ");
                System.Console.WriteLine("3-Calcular si un numero es primo o no");
                System.Console.WriteLine("4-Calcular si un numero es par o no");
                System.Console.WriteLine("5-Salir");
                opcionString= System.Console.ReadLine();
                opcion = int.Parse(opcionString);
                if (opcion < 1 || opcion > 5)
                {
                    System.Console.WriteLine("Error, introduce uno de los numeros seleccionados");
                }
            } while (opcion < 1 || opcion > 5);

            switch (opcion)
            {

                case 1:
                    System.Console.WriteLine("Introduce el numero entero que quieres calcular");
                    int numero1 = int.Parse(System.Console.ReadLine());
                    int factorial2 = factorial(numero1);
                    System.Console.WriteLine("El factorial de " + numero1 + " es " + factorial2);
                    break;

                case 2:
                    System.Console.WriteLine("Introduce un numero ");
                    int numero2 = int.Parse(System.Console.ReadLine());
                    dibujarPiramide(numero2);
                    break;

                case 3:
                    System.Console.WriteLine("Introduce el numero que quieres calcular");
                    int numero3 = int.Parse(System.Console.ReadLine());
                    int valor = esPrimo(numero3);
                    if (valor == 2)
                    {
                        System.Console.WriteLine("Es primo");
                    }
                    else
                    {
                        System.Console.WriteLine("No es primo");
                    }
                    break;

                case 4:
                    int numero4=6;
                    esPar(numero4);
                    break;

                case 5:
                    System.Console.WriteLine("Hasta la proxima");
        
                    break;
            }
            Console.WriteLine("Pulse una tecla para continuar");
            Console.ReadKey();

        }







        private static void esPar(int numero4)
        {
 
            if (numero4 % 2 == 0)
            {
                System.Console.WriteLine("Es par");
            }
            else
            {
                System.Console.WriteLine("Es impar");
            }

        }







        public static void dibujarPiramide(int numero2)
        {
            for (int linea = numero2; linea >= 1; linea--)
            {
                for (int i = 1; i <= linea; i++)
                {
                    System.Console.Write(linea);
                }
                System.Console.WriteLine();
            }
        }





        public static int esPrimo(int numero3)
        {
            int contadorRestos = 0;
            for (int i = 1; i <= numero3; i++)
            {
                if (numero3 % i == 0)
                {
                    contadorRestos++;
                }
            }

            return contadorRestos;
        }

        public static int factorial(int numero1)
        {
            int factorial = 1;
            for (int i = 1; i <= numero1; i++)
            {
                factorial = factorial * i;
            }
            return factorial;
        }







    }
}


