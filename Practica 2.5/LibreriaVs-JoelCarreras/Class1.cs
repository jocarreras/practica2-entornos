﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVs_JoelCarreras
{
    public class Class1
    {
        public static void suma(int n1, int n2)
        {
            int suma;
            suma = n1 + n2;

            System.Console.WriteLine("La suma es " + suma);
        }

        public static void potencia(int b, int exponente)
        {
            int potencia = 1;
            for (int i = 0; i < exponente; i++)
            {
                potencia = potencia * b;
            }
            System.Console.WriteLine("La potencia es  " + potencia);
        }

        public static void esPrimo(int i)
        {
            int contadorRestos = 0;
            for (int j = 1; j <= i; j++)
            {
                if (i % j == 0)
                {
                    contadorRestos++;
                }
                if (contadorRestos == 2)
                {
                    System.Console.WriteLine("Es primo");
                }
                else
                {
                    System.Console.WriteLine("No es primo");
                }
            }

        }
        public static void esPar(int numero)
        {

            for (int i = 1; i <= numero; i++)
            {
                if (numero % i == 0)
                {
                    System.Console.WriteLine("Es par");
                }
                else
                {
                    System.Console.WriteLine("Es impar");
                }
            }

        }
        public static void factorial(int n)
        {
            int factorial = 1;
            for (int i = 1; i <= n; i++)
            {
                factorial = factorial * i;
            }
            System.Console.WriteLine("");
        }







    }
}
