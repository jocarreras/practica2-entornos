﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVs_JoelCarreras
{
    public class Class2
    {
        public static void divisores(int n)
        {
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    System.Console.WriteLine(i + " es divisor");
                }
            }
        }

        public static void piramide(int n)
        {
            for (int linea = 0; linea <= n; linea++)
            {
                for (int i = 0; i <= linea; i++)
                {
                    System.Console.Write(i);
                }
                System.Console.WriteLine();
            }

        }

        public static void piramideInvertida(int n)
        {
            if (n % 2 == 0)
            {
                for (int linea = n; linea >= 0; linea--)
                {
                    for (int i = linea; i >= 0; i--)
                    {
                        System.Console.Write(i);
                    }
                    System.Console.WriteLine();
                }
            }

        }
        public static void resta(int n1, int n2)
        {

            int resta;
            resta = n1 - n2;

            System.Console.WriteLine("La resta es " + resta);

        }
        public static void division(int n1, int n2)
        {
            int division;
            division = n1 % n2;
        }
    }
}
