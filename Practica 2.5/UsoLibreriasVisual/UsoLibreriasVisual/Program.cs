﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibreriaVs_JoelCarreras;
using System.Threading.Tasks;


namespace UsoLibreriasVisual
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1.esPar(5);
            Class1.esPrimo(5);
            Class1.factorial(8);
            Class1.potencia(5, 4);
            Class1.suma(10, 15);

            Class2.division(5, 4);
            Class2.divisores(6);
            Class2.piramide(8);
            Class2.piramideInvertida(9);
            Class2.resta(10, 5);
            System.Console.ReadKey();
        }
    }
}
