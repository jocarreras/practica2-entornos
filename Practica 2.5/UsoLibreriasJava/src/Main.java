
public class Main {

	public static void main(String[] args) {
		Matematicas1.esPar(5);
		Matematicas1.esPrimo(5);
		Matematicas1.factorial(8);
		Matematicas1.potencia(5, 4);
		Matematicas1.suma(10, 15);
		
		Matematicas2.division(5, 4);
		Matematicas2.divisores(6);
		Matematicas2.piramide(8);
		Matematicas2.piramideInvertida(9);
		Matematicas2.resta(10, 5);
	}

}
