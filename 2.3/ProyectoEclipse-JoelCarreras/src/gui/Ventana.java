package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Toolkit;
import javax.swing.JSlider;
import java.awt.SystemColor;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import javax.swing.ButtonGroup;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
	/**
	 * 
	 * @author Joel
	 * @since 24-01-2018
	 */
public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/imagenes/windows-live-messenger.jpg")));
		setTitle("Messenger");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1229, 722);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnPerfil = new JMenu("Perfil");
		menuBar.add(mnPerfil);
		
		JMenu mnEstado = new JMenu("Estado");
		mnPerfil.add(mnEstado);
		
		JMenuItem mntmOnline = new JMenuItem("Online");
		mnEstado.add(mntmOnline);
		
		JMenuItem mntmOffline = new JMenuItem("Offline");
		mnEstado.add(mntmOffline);
		
		JMenu mnConfigurac = new JMenu("Configuracion");
		mnPerfil.add(mnConfigurac);
		
		JMenuItem mntmCambiarImagen = new JMenuItem("Cambiar imagen");
		mnConfigurac.add(mntmCambiarImagen);
		
		JMenuItem mntmCambiarEstado = new JMenuItem("Cambiar estado");
		mnConfigurac.add(mntmCambiarEstado);
		
		JMenuItem mntmCerrarSesion = new JMenuItem("Cerrar sesion");
		mnPerfil.add(mntmCerrarSesion);
		
		JMenu mnAmigos = new JMenu("Amigos");
		menuBar.add(mnAmigos);
		
		JMenu mnAadirUnAmigo = new JMenu("A\u00F1adir un amigo");
		mnAmigos.add(mnAadirUnAmigo);
		
		JMenuItem mntmAadirUnNumero = new JMenuItem("A\u00F1adir un numero de telefono");
		mnAadirUnAmigo.add(mntmAadirUnNumero);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Crear grupo");
		mnAmigos.add(mntmNewMenuItem);
		
		JMenu mnNewMenu = new JMenu("Herramientas");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmConfiguracionDeIdioma = new JMenuItem("Configuracion de idioma");
		mnNewMenu.add(mntmConfiguracionDeIdioma);
		
		JMenuItem mntmOpciones = new JMenuItem("Opciones...");
		mnNewMenu.add(mntmOpciones);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmAsistenciaAlCliente = new JMenuItem("Asistencia al cliente");
		mnAyuda.add(mntmAsistenciaAlCliente);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Acerca de messenger");
		mnAyuda.add(mntmNewMenuItem_1);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.menu);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(SystemColor.menu);
		tabbedPane.setBounds(15, 32, 1177, 603);
		contentPane.add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		panel_2.setForeground(new Color(70, 130, 180));
		panel_2.setToolTipText("");
		tabbedPane.addTab("Mensajes", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/mensajes privados.jpg")));
		btnNewButton_1.setBounds(403, 35, 381, 244);
		panel_2.add(btnNewButton_1);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Juan", "Miguel", "David", "Ruben", "Alicia", "Andrea"}));
		comboBox.setBounds(747, 339, 101, 37);
		panel_2.add(comboBox);
		
		JTextPane txtpnSeleccionaElContacto = new JTextPane();
		txtpnSeleccionaElContacto.setBackground(SystemColor.menu);
		txtpnSeleccionaElContacto.setForeground(SystemColor.controlText);
		txtpnSeleccionaElContacto.setText("Selecciona el contacto al que quieres mandar un mensaje");
		txtpnSeleccionaElContacto.setBounds(258, 350, 410, 26);
		panel_2.add(txtpnSeleccionaElContacto);
		
		JScrollBar scrollBar_1 = new JScrollBar();
		scrollBar_1.setBounds(1146, 0, 26, 569);
		panel_2.add(scrollBar_1);
		
		JTextArea textArea = new JTextArea();
		textArea.setBackground(SystemColor.inactiveCaptionBorder);
		textArea.setBounds(258, 423, 554, 130);
		panel_2.add(textArea);
		
		JButton btnEnviarMensaje = new JButton("Enviar mensaje");
		btnEnviarMensaje.setBounds(929, 476, 168, 29);
		panel_2.add(btnEnviarMensaje);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/Bandeja de entrada.jpg")));
		btnNewButton_2.setBounds(88, 35, 192, 174);
		panel_2.add(btnNewButton_2);
		
		JTextPane txtpnBandejaDeMensajes = new JTextPane();
		txtpnBandejaDeMensajes.setForeground(SystemColor.textHighlight);
		txtpnBandejaDeMensajes.setBackground(SystemColor.menu);
		txtpnBandejaDeMensajes.setText("Bandeja de mensajes");
		txtpnBandejaDeMensajes.setBounds(105, 244, 192, 26);
		panel_2.add(txtpnBandejaDeMensajes);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setValue(50);
		progressBar.setBounds(885, 76, 146, 14);
		panel_2.add(progressBar);
		
		JTextPane txtpnCapacidadDeLa = new JTextPane();
		txtpnCapacidadDeLa.setBackground(SystemColor.menu);
		txtpnCapacidadDeLa.setText("Capacidad de la bandeja");
		txtpnCapacidadDeLa.setBounds(885, 34, 212, 26);
		panel_2.add(txtpnCapacidadDeLa);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.menu);
		tabbedPane.addTab("A\u00F1adir contacto", null, panel_1, null);
		panel_1.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(600, 68, 169, 36);
		panel_1.add(textField);
		textField.setColumns(10);
		
		JTextPane txtpnCorreoElectronico = new JTextPane();
		txtpnCorreoElectronico.setBackground(SystemColor.menu);
		txtpnCorreoElectronico.setText("Correo electronico");
		txtpnCorreoElectronico.setBounds(406, 142, 145, 26);
		panel_1.add(txtpnCorreoElectronico);
		
		JTextPane txtpnNombreDeContacto = new JTextPane();
		txtpnNombreDeContacto.setBackground(SystemColor.menu);
		txtpnNombreDeContacto.setText("Nombre de contacto");
		txtpnNombreDeContacto.setBounds(406, 78, 158, 26);
		panel_1.add(txtpnNombreDeContacto);
		
		textField_1 = new JTextField();
		textField_1.setBounds(600, 142, 245, 26);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		buttonGroup_2.add(rdbtnHombre);
		rdbtnHombre.setBounds(406, 215, 155, 29);
		panel_1.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		buttonGroup_2.add(rdbtnMujer);
		rdbtnMujer.setBounds(614, 215, 155, 29);
		panel_1.add(rdbtnMujer);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(614, 278, 38, 26);
		panel_1.add(spinner);
		
		JTextPane txtpnEdad = new JTextPane();
		txtpnEdad.setBackground(SystemColor.menu);
		txtpnEdad.setText("Edad");
		txtpnEdad.setBounds(406, 278, 72, 26);
		panel_1.add(txtpnEdad);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/icono.jpg")));
		btnNewButton.setBounds(915, 93, 204, 200);
		panel_1.add(btnNewButton);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setIcon(new ImageIcon(Ventana.class.getResource("/imagenes/iconoamigo.png")));
		btnAgregar.setBounds(524, 397, 231, 103);
		panel_1.add(btnAgregar);
		
		JCheckBox chckbxAgregarComoFamiliar = new JCheckBox("Familiar");
		chckbxAgregarComoFamiliar.setBounds(406, 337, 129, 29);
		panel_1.add(chckbxAgregarComoFamiliar);
		
		JCheckBox chckbxAmigo = new JCheckBox("Amigo");
		chckbxAmigo.setBounds(556, 337, 139, 29);
		panel_1.add(chckbxAmigo);
		
		JCheckBox chckbxEmpresa = new JCheckBox("Empresa");
		chckbxEmpresa.setBounds(702, 337, 139, 29);
		panel_1.add(chckbxEmpresa);
		
		JTextPane txtpnEstadoOnline = new JTextPane();
		txtpnEstadoOnline.setBackground(SystemColor.menu);
		txtpnEstadoOnline.setBounds(1061, 0, 56, 29);
		contentPane.add(txtpnEstadoOnline);
		txtpnEstadoOnline.setText("Estado: ");
		
		JSlider slider_1 = new JSlider();
		slider_1.setPaintTicks(true);
		slider_1.setPaintLabels(true);
		slider_1.setMaximum(150);
		slider_1.setMinimum(50);
		slider_1.setMajorTickSpacing(50);
		slider_1.setBounds(789, 3, 238, 51);
		contentPane.add(slider_1);
		
		JTextPane txtpnCambiarTamaoDe = new JTextPane();
		txtpnCambiarTamaoDe.setForeground(Color.BLACK);
		txtpnCambiarTamaoDe.setBackground(SystemColor.menu);
		txtpnCambiarTamaoDe.setText("Cambiar tama\u00F1o de la letra");
		txtpnCambiarTamaoDe.setBounds(577, 3, 205, 26);
		contentPane.add(txtpnCambiarTamaoDe);
		
		JTextPane txtpnOnline = new JTextPane();
		txtpnOnline.setForeground(new Color(0, 128, 0));
		txtpnOnline.setBackground(SystemColor.menu);
		txtpnOnline.setText("Online");
		txtpnOnline.setBounds(1116, 0, 63, 26);
		contentPane.add(txtpnOnline);
	}
}
